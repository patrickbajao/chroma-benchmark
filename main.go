package main

import (
	"fmt"
	"io"
	"os"

	"github.com/alecthomas/chroma/v2/quick"
)

func main() {
	data, err := os.ReadFile("index.json")
	if err != nil {
		fmt.Println("failed to read")
	}

	Highlight(os.Stdout, string(data), "json")
}

func Highlight(w io.Writer, content string, language string) {
	err := quick.Highlight(w, content, language, "html", "monokai")
	if err != nil {
		fmt.Println("error")
	}
}
