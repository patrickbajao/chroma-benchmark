package main

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
	"testing"
)

func BenchmarkHighlightJson(b *testing.B) {
	data, err := os.ReadFile("index.json")
	if err != nil {
		fmt.Println("failed to read")
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		var b bytes.Buffer
		out := bufio.NewWriter(&b)
		Highlight(out, string(data), "json")
	}
}

func BenchmarkHighlightRuby(b *testing.B) {
	data, err := os.ReadFile("merge_request.rb")
	if err != nil {
		fmt.Println("failed to read")
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		var b bytes.Buffer
		out := bufio.NewWriter(&b)
		Highlight(out, string(data), "ruby")
	}
}
