module gitlab.com/patrickbajao/chroma-benchmark

go 1.21.4

require github.com/alecthomas/chroma/v2 v2.10.0

require github.com/dlclark/regexp2 v1.10.0 // indirect
